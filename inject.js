const injectScript = (file_path, tag, data) => {
    const node = document.getElementsByTagName(tag)[0];
    const script = document.createElement('script');
    script.setAttribute('type', 'text/javascript');
    script.setAttribute('src', file_path);
    script.dataset.url = data;
    node.appendChild(script);
}
injectScript(chrome.runtime.getURL('content.js'), 'body', chrome.runtime.getURL('ru_flag_grey.png'));

window.addEventListener('load', () => {
    chrome.storage.sync.get(['enableProMode', 'showNearestCard'], (data) => {
        window.postMessage(data, 'https://boardgamearena.com');
    });
})

chrome.runtime.onMessage.addListener((data, sender, sendResponse) => {
    chrome.storage.sync.get(['enableProMode', 'showNearestCard'], (data) => {
        window.postMessage(data, 'https://boardgamearena.com');
    });
    sendResponse(true);
});



