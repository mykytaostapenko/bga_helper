window.addEventListener('load', () => {
    const showNearestCard = document.querySelector('#showNearestCard');
    const enableProMode = document.querySelector('#enableProMode');

    chrome.storage.sync.get(['enableProMode', 'showNearestCard'], (data) => {
        showNearestCard.checked = data.showNearestCard ?? false;
        showNearestCard.parentNode.style.display = data.enableProMode ? 'flex' : 'none';
        enableProMode.checked = data.enableProMode ?? false;
    });

    showNearestCard.addEventListener('change', (e) => {
        chrome.storage.sync.get(['enableProMode', 'showNearestCard'], (data) => {
            chrome.storage.sync.set({ ...data, showNearestCard: e.target.checked });
        });

        sendMessage(e.target.checked);
    });

    enableProMode.addEventListener('change', (e) => {
        console.log('HELLO');
        chrome.storage.sync.get(['enableProMode', 'showNearestCard'], (data) => {
            chrome.storage.sync.set({ ...data, enableProMode: e.target.checked });
        });
        sendMessage(e.target.checked);
    });

    const sendMessage = (data) => {
        chrome.tabs.query({url: ["https://boardgamearena.com/*"]}, (tabs) => {
            if (!!tabs.length) {
                tabs.map((tab) => {
                    chrome.tabs.sendMessage(tab.id, data, () => {
                        chrome.storage.sync.get(['enableProMode'], (data) => {
                            showNearestCard.parentNode.style.display = data.enableProMode ? 'flex' : 'none';
                        });
                    });
                });
            }
        });
    }
});


