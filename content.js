window.addEventListener('load', () => {
	window.addEventListener('popstate', (e) => {
		setTimeout(() => {
			makeRusnyaShitAgain();
		}, 1000);
	});
	document.querySelector('#ebd-body')?.addEventListener('click', (e) => {
		if (e.target.classList.contains('savelifeDonate')) {
			return;
		}
		if (!!e.target.href) {
			setTimeout(() => {
				makeRusnyaShitAgain();
			}, 1000);
		}

		let currentElement = e.target;
		let hasAnchorParent = false;

		while (currentElement.parentElement) {
			currentElement = currentElement.parentElement;

			if (currentElement.tagName === 'A' || currentElement.classList.contains('bga-matchmaking-list-item')) {
				hasAnchorParent = true;
				break;
			}
		}

		if (hasAnchorParent) {
			setTimeout(() => {
				makeRusnyaShitAgain();
			}, 1000);
		}
	});
	document.querySelector('.bga-menu-bar__logo-holder')?.addEventListener('click', (e) => {
		setTimeout(() => {
			makeRusnyaShitAgain();
		}, 1000);
	});

	const makeRusnyaShitAgain = () => {
		const ruzzia_FLAG_COORDS = '-288px -88px';
		const ruShitImg = Array.from(document.scripts).filter((script) => {
			return script.dataset.url?.includes('ru_flag_grey.png');
		})[0];
		const filterFlags = (flags, block) => {
			flags.filter((flagNode) => {
				return flagNode.style.backgroundPosition === ruzzia_FLAG_COORDS;
			}).map((flagNode) => {
				makeCorrectFlags(flagNode, block);
			});
		}
		const makeCorrectFlags = (flagNode, block) => {
			const parentFlexArr = ['playersRating', 'personalPage', 'gameInfoPageRating', 'pagePrestigePlayers', 'tableCreationPlayers'];

			if (block === 'personalPage' || block === 'tooltipBlock' || block === 'tableCreationPlayers') {
				const parentNodeChildrens = Array.from(flagNode.parentNode.childNodes);
				parentNodeChildrens.map((node) => {
					if (node.nodeName === '#text' || node.nodeName === 'SPAN' || node.nodeName === 'P' || node.classList.contains('fa')) {
						flagNode.parentNode.removeChild(node);
					}
				});
			}

			flagNode.style.backgroundImage = `url(${ruShitImg.dataset.url})`;
			flagNode.style.backgroundPosition = 'center';
			flagNode.style.backgroundSize = 'cover';
			flagNode.style.minWidth = '30px';
			flagNode.style.height = '30px';
			flagNode.style.position = 'relative';
			flagNode.style.top = '-6px';
			flagNode.style.left = '-7px';

			const div = document.createElement('div');
			const span = document.createElement('span');
			const link = document.createElement('a');

			div.style.cssText = 'position: relative; top: -6px';

			span.innerHTML = '<small>r</small>uzzia is a terrorist state!'
			span.style.cssText = 'display: block; color: red; font-size: 12px;';

			link.innerText = 'Click to donate to Ukraine charity fund!🇺🇦';
			link.style.cssText = 'font-weight: bold; font-size: 16px; color: rgba(0, 87, 183, 1);';
			link.href = 'https://savelife.in.ua/en/donate-en/#donate-army-card-monthly';
			link.target = '_blank';
			link.classList.add('savelifeDonate');

			if (parentFlexArr.includes(block)) {
				flagNode.parentNode.style.display = 'flex';
			}

			if (block === 'gameInfoPageRating') {
				flagNode.parentNode.style.display = 'none';
				flagNode.parentNode.style.width = 'max-content';
				flagNode.parentNode.style.maxWidth = '280px';
				flagNode.parentNode.style.position = 'relative';
				flagNode.parentNode.style.left = '-50px';
				flagNode.parentNode.style.top = '-5px';
				flagNode.parentNode.style.margin = '10px 0';
				flagNode.parentNode.lastChild.style.marginLeft = '20px';
				flagNode.nextSibling.nextSibling.remove();
			}

			if (block === 'tableCreationPlayers') {
				flagNode.parentNode.style.minWidth = 'max-content';
				let parent = flagNode.parentNode;

				while (!parent.classList.contains('active_player_left')) {
					parent = parent.parentNode;
				}

				parent.style.width = '100%';
				parent.querySelector('.playersubinfos_lang').remove();
			}

			if (block === 'gameZoneRightBlock' || block === 'tooltipBlock') {
				flagNode.style.position = 'none';
				flagNode.style.top = '0';
				flagNode.style.left = '0';
				div.style.cssText = 'margin: 10px 0';
			}

			if (block === 'playersRating' || block === 'pagePrestigePlayers') {
				flagNode.parentNode.style.width = 'calc(100% - 110px)';
				flagNode.parentNode.parentNode.style.height = 'auto';
				flagNode.parentNode.style.top = '-10px';
			}

			if (block === 'mainPrestigePlayers') {
				flagNode.parentNode.style.height = 'auto';
				flagNode.parentNode.style.margin = '0 0 10px 10px';
				flagNode.style.position = 'none';
				flagNode.style.top = '0';
				flagNode.style.left = '0';
				div.style.cssText = 'margin-top: 5px;';
			}

			div.append(span);
			div.append(link);

			flagNode.parentNode.append(div);
		}
		const makeListenerOnClick = (node, func) => {
			document.querySelector(node).addEventListener('click', () => {
				setTimeout(() => {
					func();
				}, 1000);
			});
		}

		const flagFromTooltipObserver = new MutationObserver( () => {
			const flag = document.querySelector('.dijitTooltipContainer .flag');

			if (!!flag && flag.style.backgroundPosition === ruzzia_FLAG_COORDS) {
				makeCorrectFlags(flag, 'tooltipBlock');
			}
		});
		const checkTooltipObserver = () => {
			if (!!document.querySelector('.dijitTooltipContainer')) {
				flagFromTooltipObserver.observe(document.querySelector('.dijitTooltipContainer'), {childList: true});
			} else {
				setTimeout(() => {
					checkTooltipObserver();
				}, 1000);
			}
		}
		checkTooltipObserver();

		if (!!document.querySelector('.bga-game-panel__grid')) {
			const rusnyaFlagsCreate = () => {
				const allRatingBlocks = Array.from(document.querySelectorAll('.bga-game-panel__grid [slot="subtitle"]'));
				allRatingBlocks.map((blockWithFlag) => {
					const rusnyaShitNode = blockWithFlag.firstChild.cloneNode(true);
					blockWithFlag.append(rusnyaShitNode);
					makeCorrectFlags(rusnyaShitNode.querySelector('.flag'), 'gameInfoPageRating');
				});
				rusnyaFlagsCheck();
			}
			const rusnyaFlagsCheck = () => {
				const allRatingBlocks = Array.from(document.querySelectorAll('.bga-game-panel__grid [slot="subtitle"]'));
				allRatingBlocks.map((blockWithFlag) => {
					if (blockWithFlag.firstChild.firstChild.style.backgroundPosition === ruzzia_FLAG_COORDS) {
						blockWithFlag.firstChild.style.display = 'none';
						blockWithFlag.lastChild.style.display = 'flex';
					} else {
						blockWithFlag.firstChild.style.display = 'flex';
						blockWithFlag.lastChild.style.display = 'none';
					}
				});
			}

			setTimeout(() => {
				rusnyaFlagsCreate();
			}, 1000);
			setTimeout(() => {
				Array.from(document.querySelectorAll('.block-panel-rankings .grid .cursor-pointer')).map((node) => {
					node.addEventListener('click', () => {
						setTimeout(() => {
							rusnyaFlagsCheck();
						}, 1000)
					});
				});
			}, 1000);
		}

		if (!!document.querySelector('#welcomebigprestige_players')) {
			const flagNodes = Array.from(document.querySelectorAll('#welcomebigprestige_players .flag'));
			filterFlags(flagNodes, 'mainPrestigePlayers');
		}

		if (!!document.querySelector('#right-side-first-part')) {
			const flagNodes = Array.from(document.querySelectorAll('#right-side-first-part .flag'));
			filterFlags(flagNodes, 'gameZoneRightBlock');
		}

		if (!!document.querySelector('#pagesection_publicinfos')) {
			const flagNode = document.querySelector('#pagesection_publicinfos .flag');
			if (flagNode.style.backgroundPosition === ruzzia_FLAG_COORDS) {
				makeCorrectFlags(flagNode, 'personalPage');
			}
		}

		if (!!document.querySelector('#prestige_more_players')) {
			const findFlags = () => {
				const flagNodes = Array.from(document.querySelectorAll('#prestige_more_players .flag'));
				filterFlags(flagNodes, 'pagePrestigePlayers');
			}
			findFlags();
			makeListenerOnClick('#prestige_see_more', findFlags);
		}

		if (!!document.querySelector('#players')) {
			const findFlags = () => {
				const flagNodes = Array.from(document.querySelectorAll('#players .flag'));
				filterFlags(flagNodes, 'pagePrestigePlayers');
			}
			findFlags();
			makeListenerOnClick('#seemore_rankings', findFlags);
		}

		if (!!document.querySelector('#players_at_table')) {
			const flagNodes = Array.from(document.querySelectorAll('#players_at_table .flag'));
			filterFlags(flagNodes, 'tableCreationPlayers');
		}

		if (!!document.querySelector('#mainRanking')) {
			const findFlags = () => {
				const flagNodes = Array.from(document.querySelectorAll('#ownRanking .flag'));
				const flagNodesMain = Array.from(document.querySelectorAll('#mainRanking .flag'));
				filterFlags(flagNodes, 'pagePrestigePlayers');
				filterFlags(flagNodesMain, 'pagePrestigePlayers');
			}
			findFlags();
			makeListenerOnClick('#header .next-season', findFlags);
			makeListenerOnClick('#header .previous-season', findFlags);
		}
	}
	setTimeout(() => {
		makeRusnyaShitAgain();
	}, 1000)

	//GAME INTERFACE BLOCK
	if (document.body.classList.contains('game_interface')) {
		const playersZone = document.querySelector('#player_boards');
		const gamesId = document.querySelector('.gamerank_no')?.id.replace('gamerank_no_', '');

		const initPlayerInfo = (playerInfoBlock) => {
			const infoBlock = document.createElement('div');
			const gamesPlayedBlock = document.createElement('span');
			const gamesWonBlock = document.createElement('span');
			const averagePointsBlock = document.createElement('span');

			infoBlock.style.cssText = 'font-size: 14px; margin: 5px 0; padding: 5px;'
			infoBlock.classList.add('game_player_info');

			gamesPlayedBlock.classList.add('games-played');
			gamesWonBlock.classList.add('games-won');
			averagePointsBlock.classList.add('games-avr-points');

			infoBlock.append(gamesPlayedBlock);
			infoBlock.append(gamesWonBlock);
			infoBlock.append(averagePointsBlock);

			playerInfoBlock.append(infoBlock);
		}

		if (!!gamesId) {
			Array.from(playersZone.querySelectorAll('.player-name')).map((player) => {
				const id = player.id.replace('player_name_', '');
				const playerInfoBlock = document.querySelector(`#overall_player_board_${id}`);
				initPlayerInfo(playerInfoBlock);

				const headers = new Headers({
					'x-request-token': window.bgaConfig.requestToken
				});
				fetch(`https://boardgamearena.com/gamestats/gamestats/getGames.html?player=${id}&game_id=${gamesId}&opponent_id=0&updateStats=1`,
					{
						method: 'GET',
						headers: headers
					}
				).
					then((response) => response.json()).
					then((response) => makePlayerInfo(id, response, playerInfoBlock)).
					catch((err) => console.log(err));
			});
		}

		const makePlayerInfo = (playerId, playerInfo, playerInfoBlock) => {
			const playerInfoToRender = {};
			playerInfoToRender.id = playerId;
			playerInfoToRender.playerInfoBlock = playerInfoBlock;
			playerInfoToRender.gamesPlayed = playerInfo.data.stats.general.played ?? 0;
			playerInfoToRender.gamesWon = playerInfo.data.stats.general.victory ?? 0;
			playerInfoToRender.avaragePoints = playerInfo.data.stats.general.score ?? 0;

			renderPlayerInfo(playerInfoToRender);
		}

		const renderPlayerInfo = (playerInfoToRender) => {
			const gamesPlayedBlock = document.querySelector(`#overall_player_board_${playerInfoToRender.id} .games-played`);
			const gamesWonBlock = document.querySelector(`#overall_player_board_${playerInfoToRender.id} .games-won`);
			const averagePointsBlock = document.querySelector(`#overall_player_board_${playerInfoToRender.id} .games-avr-points`);

			gamesPlayedBlock.innerText = `Games played: ${playerInfoToRender.gamesPlayed}\n`;
			gamesWonBlock.innerText = `Games won: ${playerInfoToRender.gamesWon}\n`;
			averagePointsBlock.innerText = `Average points: ${playerInfoToRender.avaragePoints}\n`;

			playerInfoToRender.playerInfoBlock.querySelector('.player_elo_wrap').style.cssText = 'visibility: visible;'
		}
	}

	//6 NIMMT!
	if (window.location.pathname.includes('sechsnimmt')) {
		let popupData = false;
		let remainingCards = [];
		let handCards = [];
		let tableCards = [];
		let releasedCards = [];
		const gameZone = document.querySelector('#gamespace_wrap');
		const overallContentBlock = document.querySelector('#overall-content');

		window.addEventListener('message', (e) => {
			popupData = e.data;
		});
		//Added hover listener on hand cards to find nearest cards
		document.querySelector('#player_hand').addEventListener('mouseover', (e) => {
			const targetId = e.target.id;

			if (targetId.includes('player_hand_item')) {
				const filterAndSortRowCards = (cardNodes, topFilter) => {
					return cardNodes.filter((card) => {
						return card.style?.top === topFilter;
					}).sort((a, b) => {
						return Number(a.style.left.replace('px', '')) - Number(b.style.left.replace('px', ''));
					});
				}

				const newTableCards = Array.from(document.querySelector('#cards_on_table').childNodes);
				const leftPart = [];
				const lastPart = [];
				const handCardId = Number(targetId.replace('player_hand_item_', ''));
				const row1 = filterAndSortRowCards(newTableCards, '3px');
				const row2 = filterAndSortRowCards(newTableCards, '121px');
				const row3 = filterAndSortRowCards(newTableCards, '239px');
				const row4 = filterAndSortRowCards(newTableCards, '357px');

				leftPart.push(row1[0], row2[0], row3[0], row4[0]);
				lastPart.push(row1[row1.length - 1], row2[row2.length - 1], row3[row3.length - 1], row4[row4.length - 1])

				const leftColumnCards = getCards(leftPart, 'card_').map((cardNum) => Number(cardNum));
				const lastCardsInRow = getCards(lastPart, 'card_').map((cardNum) => Number(cardNum));

				const nearestSmallerNumber = leftColumnCards.reduce((acc, cardNum) => {
					if ((handCardId < cardNum && cardNum < acc) || (handCardId < cardNum && acc === 0)) {
						return cardNum;
					}
					return acc;
				}, 0);
				const nearestLargerNumber = lastCardsInRow.reduce((acc, cardNum) => {
					if ((handCardId > cardNum && cardNum > acc) || (handCardId > cardNum && acc === 0)) {
						return cardNum;
					}
					return acc;
				}, 0);

				const nearestLargerNumberCard = document.querySelector(`#card_content_${nearestLargerNumber}`);
				const nearestSmallerNumberCard = document.querySelector(`#card_content_${nearestSmallerNumber}`);
				if (!popupData.enableProMode) {
					nearestLargerNumberCard?.classList.add('table_card_hover', 'blue');
					highlightCardsRange({playerCard: handCardId, rightCard: nearestLargerNumber});
				} else if (nearestSmallerNumber > 0 && nearestLargerNumber > 0 && popupData.showNearestCard) {
					if ((Math.abs(handCardId - nearestSmallerNumber)) === (Math.abs(handCardId - nearestLargerNumber))) {
						nearestLargerNumberCard?.classList.add('table_card_hover', 'blue');
						nearestSmallerNumberCard?.classList.add('table_card_hover', 'yellow');
					} else {
						if (Math.abs(handCardId - nearestSmallerNumber) > Math.abs(handCardId - nearestLargerNumber)) {
							document.querySelector(`#card_content_${nearestLargerNumber}`)?.classList.add('table_card_hover', 'blue');
							highlightCardsRange({playerCard: handCardId, rightCard: nearestLargerNumber});
						} else {
							document.querySelector(`#card_content_${nearestSmallerNumber}`)?.classList.add('table_card_hover', 'yellow');
							highlightCardsRange({playerCard: handCardId, leftCard: nearestSmallerNumber});
						}
					}
				} else {
					nearestLargerNumberCard?.classList.add('table_card_hover', 'blue');
					nearestSmallerNumberCard?.classList.add('table_card_hover', 'yellow');
					highlightCardsRange({playerCard: handCardId, rightCard: nearestLargerNumber, leftCard: nearestSmallerNumber});
				}


			}
		});

		const highlightCardsRange = (cardsInfo) => {
			if (!!cardsInfo.rightCard && cardsInfo.rightCard > 0) {
				Array.from(document.querySelectorAll('.remaining span')).map((span) => {
					if (span.textContent > cardsInfo.rightCard && span.textContent < cardsInfo.playerCard) {
						span.style = 'color: dodgerblue;';
					}
				});
			}
			if (!!cardsInfo.leftCard && cardsInfo.leftCard > 0) {
				Array.from(document.querySelectorAll('.remaining span')).map((span) => {
					if (span.textContent < cardsInfo.leftCard && span.textContent > cardsInfo.playerCard) {
						span.style = 'color: yellow;';
					}
				});
			}

			if ((!!cardsInfo.leftCard && cardsInfo.leftCard > 0) && (!!cardsInfo.rightCard && cardsInfo.rightCard > 0)) {
				const remainingBlock = document.querySelector('#game_play_area .remaining');
				const diffBlock = document.createElement('div');
				const upBlock = document.createElement('div');
				const downBlock = document.createElement('div');
				upBlock.style = 'color: #ffff16;';
				downBlock.style = 'color: dodgerblue;';
				upBlock.innerText = `To left card: ${cardsInfo.leftCard - cardsInfo.playerCard}`;
				downBlock.innerText = `To right card: ${cardsInfo.playerCard - cardsInfo.rightCard}`;
				diffBlock.append(upBlock, downBlock);
				diffBlock.classList.add('cards-diff');
				diffBlock.style = 'margin: 5px 0; font-size: 14px;'
				remainingBlock.prepend(diffBlock);
			}
		}

		//removed classes from nearest cards on table
		document.querySelector('#player_hand').addEventListener('mouseout', () => {
			const tableCardWithHoverClasses = Array.from(document.querySelectorAll('.table_card_hover'));
			tableCardWithHoverClasses.map((node) => node.classList.remove('table_card_hover', 'blue', 'yellow'));
			Array.from(document.querySelectorAll('.remaining span')).map((span) => {
				span.style = '';
			});
			const cardsDiffBlock = document.querySelector('#game_play_area .remaining .cards-diff');
			cardsDiffBlock?.parentNode.removeChild(cardsDiffBlock);
		});

		const getCards = (cards, replaceString) => {
			return cards.map((card) => {
				if (!!card.id) {
					return card.id.replace(replaceString, '');
				}

				return 0;
			}).filter((card) => card !== 0);
		}

		const renderCards = (gameZone, handCards, tableCards, releasedCards, remainingCardsArr) => {
			const remainingCardsBlock = document.createElement('div')
			const remainingCardsTitle = document.createElement('h3');
			remainingCardsTitle.textContent = `Remaining cards: \n`;
			remainingCardsBlock.append(remainingCardsTitle);

			remainingCardsArr.map((num) => {
				const span = document.createElement('span');
				span.textContent = num;
				remainingCardsBlock.append(span, ', \n');
				return num;
			});

			if (gameZone.classList.contains('info-added')) {
				const remainingBlock = gameZone.querySelector('.remaining');
				const releasedBlock = gameZone.querySelector('.released');

				remainingBlock.innerHTML = '';
				remainingBlock.append(remainingCardsBlock);
				releasedBlock.innerHTML = `<h3>Released cards:</h3> \n ${releasedCards}`;

				if (overallContentBlock.classList.contains('gamestate_cardChoice')) {
					remainingBlock.style.top = '0';
					releasedBlock.style.top = '0';
				}

				return;
			}

			const remainingCardsDiv = document.createElement('div');
			const releasedCardsSpan = document.createElement('span');
			remainingCardsDiv.classList.add('remaining');
			releasedCardsSpan.classList.add('released');
			remainingCardsDiv.append(remainingCardsBlock);
			releasedCardsSpan.innerHTML = `<h3>Released cards:</h3> \n ${releasedCards}`;
			remainingCardsDiv.style.cssText = 'color: #2ea83a; position: absolute; top: 0; left: 500px; z-index: 1; width: 105px; font-size: 16px; font-weight: bold; background-color: black;';
			releasedCardsSpan.style.cssText = 'color: #e52e3a; position: absolute; top: 0; left: 620px; z-index: 1; width: 105px; font-size: 16px; font-weight: bold; background-color: white;';

			gameZone.append(remainingCardsDiv);
			gameZone.append(releasedCardsSpan);
			gameZone.classList.add('info-added');
		}

		const makeRightHeadNumber = (cardId) => {
			const num = parseInt(cardId);
			if (num === 55) {
				return 7;
			} else if ((num % 5) === 0 && (num % 10) !== 0) {
				return 2;
			} else if ((num % 10) === 0) {
				return 3;
			} else if ((num % 11) === 0) {
				return 5;
			} else {
				return 1;
			}
		}

		const renderHeadsInRow = (heads) => {
			const headsCount = Object.values(heads);

			for (let i = 1; i < 5; i++) {
				const rowHeadSpan = document.querySelector(`#row_${i} .row-head-count`);

				if (!!rowHeadSpan) {
					rowHeadSpan.innerText = `${headsCount[i - 1]}`;
					continue;
				}

				const headSpan = document.createElement('span');
				headSpan.classList.add('row-head-count');
				headSpan.innerText = `${headsCount[i - 1]}`;
				headSpan.style.cssText = 'font-size: 28px;';
				document.querySelector(`#row_${i} .lastplace`).append(headSpan);
			}
		}

		const calculateHeadsInRow = (cardsOnTable) => {
			const heads = cardsOnTable.map((card) => ({position: card.offsetTop ?? null, num: card.id?.replace('card_', '') ?? null}))
				.filter((card) => !!card.position)
				.reduce((acc, card) => {
					if (acc[card.position]) {
						return {...acc, [card.position]: acc[card.position] + makeRightHeadNumber(card.num)};
					}
					return {...acc, [card.position]: makeRightHeadNumber(card.num)};
				}, {});

			renderHeadsInRow(heads);
		}

		const makeCards = () => {
			let oldTableCards = [...tableCards];
			const newTableCards = Array.from(document.querySelector('#cards_on_table').childNodes);
			if (overallContentBlock.classList.contains('gamestate_cardChoice') ||
					overallContentBlock.classList.contains('gamestate_smallestCard')
			) {
				calculateHeadsInRow(newTableCards);
			}

			handCards = getCards(Array.from(document.querySelector('#player_hand').childNodes), 'player_hand_item_');
			tableCards = getCards(newTableCards, 'card_');
			oldTableCards = oldTableCards.filter((card) => {
				return !tableCards.includes(card);
			});
			releasedCards = [...releasedCards, ...oldTableCards];

			remainingCardsArr = [];
			for (let i = 1; i < 105; i++) {
				const num = `${i}`;
				if (tableCards.includes(String(num)) || releasedCards.includes(num) || handCards.includes(num)) {
					continue;
				}

				remainingCardsArr.push(i);
			}

			renderCards(gameZone, handCards, tableCards, releasedCards.sort((a, b) => a - b).join(', '), remainingCardsArr);
		}

		const resetCards = () => {
			remainingCards = [];
			handCards = [];
			tableCards = [];
			releasedCards = [];
		};

		const addPlayerPlayedCardsInfo = () => {
			Array.from(document.querySelectorAll('#played .card_played')).map((player) => {
				const playerName = player.querySelector('.card_played_player').textContent.slice(0, -1);
				const playerCard = player.querySelector('.card_content').id.replace('card_content_', '');

				Array.from(document.querySelectorAll('#player_boards .player-name a')).map((playerBoardName) => {
					if (playerBoardName.textContent === playerName) {
						const playerInfoBlock = playerBoardName.parentNode.parentNode.parentNode.querySelector('.game_player_info');

						if (playerInfoBlock.lastChild.classList.contains('player_played_cards')) {
							const playedCards = playerInfoBlock.lastChild.textContent === '' ? [] : playerInfoBlock.lastChild.textContent.split(', ');
							if (!playedCards.includes(playerCard)) {
								playedCards.push(playerCard);
								playerInfoBlock.lastChild.textContent = playedCards.sort((a, b) => a - b).join(', ');
							}
						} else {
							const spanPlayedCards = document.createElement('span');
							const spanTitlePlayedCards = document.createElement('span');
							spanTitlePlayedCards.textContent = 'Played cards: '
							spanPlayedCards.textContent = playerCard;
							spanPlayedCards.classList.add('player_played_cards');
							spanPlayedCards.style.cssText = 'color: red; font-weight: bold;'
							playerInfoBlock.append(spanTitlePlayedCards);
							playerInfoBlock.append(spanPlayedCards);
						}
					}
					return playerBoardName;
				});
			});
		}

		const resetPlayedCardsInfo = () => {
			Array.from(document.querySelectorAll('.player_played_cards')).map((infoBlock) => {
				infoBlock.textContent = '';
			});
			addPlayerPlayedCardsInfo();
		}

		// Make initial cards after game is load
		makeCards();

		const gameStateObserver = new MutationObserver(() => {
			if (document.querySelector('#player_hand').childElementCount === 10) {
				resetCards();
				resetPlayedCardsInfo();
			}
			else if (
				overallContentBlock.classList.contains('gamestate_cardPlace') ||
				overallContentBlock.classList.contains('gamestate_smallestCard')
			) {
				gameZone.querySelector('.remaining').style.top = '460px';
				gameZone.querySelector('.released').style.top = '460px';
				addPlayerPlayedCardsInfo();
			}
			else if (overallContentBlock.classList.contains('gamestate_cardChoice')) {
				makeCards();
			}
			else if (overallContentBlock.classList.contains('gamestate_gameEnd')) {
				gameStateObserver.disconnect();
				cardsChangedObserver.disconnect();
				console.log("KINEC");
			}
		});
		gameStateObserver.observe(overallContentBlock, {attributes: true});

		const cardsChangedObserver = new MutationObserver (() => {
			makeCards();
		});
		cardsChangedObserver.observe(document.querySelector('#cards_on_table'), {childList: true});
	}

	if (window.location.pathname.includes('downforce')) {}
});
